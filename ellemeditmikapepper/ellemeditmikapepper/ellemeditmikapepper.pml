<?xml version="1.0" encoding="UTF-8" ?>
<Package name="ellemeditmikapepper" format_version="4">
    <Manifest src="manifest.xml" />
    <BehaviorDescriptions>
        <BehaviorDescription name="behavior" src="ellemeditmikapepper" xar="behavior.xar" />
    </BehaviorDescriptions>
    <Dialogs />
    <Resources>
        <File name="icon" src="icon.png" />
        <File name="Mika - Elle Me Dit (clip officiel)" src="Mika - Elle Me Dit (clip officiel).mp3" />
        <File name="cover" src="html/cover.jpg" />
    </Resources>
    <Topics />
    <IgnoredPaths>
        <Path src=".metadata" />
    </IgnoredPaths>
</Package>
